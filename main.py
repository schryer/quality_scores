import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from mpld3_plugins import PointHTMLTooltip, InteractiveLegendPlugin, plugins, mpld3

from plotarray import make_plot_array, save_plot_data, retrieve_plot_data, make_colour_map, colour_dic, colours

N = 51
df = pd.DataFrame(index=range(N))

quality_keys = ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9']
for qk in quality_keys:
    df[qk] = np.random.randn(N)

rows = []
for i in df.index:
    row = df.ix[[i], :].T
    row.columns = ['Month {0}'.format(i)]

    # .to_html() is unicode; so make leading 'u' go away with str()
    rows.append(str(row.to_html()))


fig, ax = plt.subplots(figsize=(12, 8))
ax.grid(True, alpha=0.3)

colour_map = make_colour_map(1, len(quality_keys), 'Accent')
all_points = []
for index, qk in enumerate(quality_keys):
    plot_colour = colour_map.to_rgba(index)

    points = ax.plot(df.index, df[qk], 'o', color=plot_colour, linestyle='-',
                     linewidth=2,
                     mec=plot_colour, ms=4, mew=1, alpha=0.1)

    plugins.connect(fig, PointHTMLTooltip(points[0], rows))
    all_points.append(points)

fontsize = 24
ax.set_xlabel('Time (months)', size=fontsize)
ax.set_ylabel('Quality value', size=fontsize)
ax.set_title('Quality scores over time', size=fontsize)

interactive_legend = InteractiveLegendPlugin(all_points,
                                             quality_keys,
                                             alpha_unsel=0.1)
plugins.connect(fig, interactive_legend)

fn = 'quality_figure.html'
print('Saving: {}'.format(fn))
with open(fn, 'w') as f:
    mpld3.save_html(fig, f)
